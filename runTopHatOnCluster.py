#!/usr/bin/env python


"Writes out qsub scripts for running TopHat in a cluster computing environment that uses PBS to schedule jobs."

import os,sys,optparse

def getFastqNames(dir='fastq',ext=None):
    """
    Function: Get list of FASTQ files to align
              Files must have suffix .fastq or .fastq.gz
    Args    : dir - string, name of directory with FASTQ files
    Returns : list of files

    TopHat output directories will be named for the FASTQ file suffixes.
    ex) Control1.fastq output will be in Control1
    """
    files = os.listdir(dir)
    newfiles = []
    for f in files:
        if ext:
            if f.endswith(ext):
                newfiles.append(dir+'/'+f)
        else:
            if f.endswith('fastq') or f.endswith('fastq.gz'):
                newfiles.append(dir+'/'+f)
    return newfiles

def writeSubmissionScripts(files=None,
                           run_name=None,
                           base_dir=None,
                           quals=None,
                           processors_per_node=8, # always run on one node
                           vmem=96000,
                           max_intron_size=2000,
                           min_intron_size=50,
                           segment_length=None,
                           paired_end=False,
                           read_distance=None,
                           index=None):
    """
    Function: Write job submission scripts and master qsub script
    Args    : files - output from getFastqNames
              run_name - name of the run, TopHat output directories will be saved in
                         a directory named run_name [required]
              base_dir - directory where the TopHat output directories will reside
                         if not provided, this will be the same directory where the
                         qsub command is run ([optional, defaults to $PBS_O_WORKDIR])
              quals - quality score scale, uses --solexa-quals for pre-1.3 Illumina, --solexa1.3-quals for 1.3 
              processors_per_node - int, number of processors to use per TopHat invocation,
                                    TopHat -p parameter [default is 6]
              vmem - int, megabytes of memory each node can use [default is 16,000]
              max_intron_size - max intron to predict, see getIntrons.py [default is 2000]
              min_intron_size - min intron to predict, see getIntrons.py [default is 50]
              index - location and name of BowTie index 
              segment_length - should be about half the read length; use default or reads 50 bases
                               or more [uses TopHat default if not given]
    Returns : NA

      Qsub master submission script will be named for the run name.
        ex) Script for run name EXP1 will be named EXP1.sh
        
      Job scripts will be named using the FASTQ file prefixes, 
        ex) Control1.fastq job submission script will be called Control1.sh
    """
    # first check to see if run_name directory already exists
    # if it exists and isn't empty, don't proceed
    # if it exists and is empty, continue
    # if it doesn't exist, create it and continue
    try:
        test = os.listdir(run_name)
        if len(test)>0:
            raise OSError("Can't create new folder for run, %s exists and is not empty."%run_name)
    except OSError:
        os.mkdir(run_name) # will fail if there are permission problems, that's OK
    qsub_script = run_name + '.sh'
    qsub_fh = open(qsub_script,'w')
    qsub_fh.write('#!/bin/bash\n')
    for f in files:
        filename = f.split('/')[-1]
        if f.endswith('fastq.gz'):
            prefix = '.'.join(filename.split('.')[0:-2])
            suffix = '.fastq.gz'
        else:
            prefix = '.'.join(filename.split('.')[0:-1])
            suffix = '.fastq'
        fn = f
        if paired_end:
            if prefix.endswith('_2'):
                continue
            else:
                if prefix.endswith('_1'):
                    prefix = prefix.split('_1')[0]
                    d = '/'.join(f.split('/')[0:-1])
                    f = d + '/' + prefix + '_1' + suffix + " " + d + '/' + prefix + '_2' + suffix
                else:
                    raise Error("Can't recognize names for paired end files.")
        job_submission_script = prefix+'.sh'
        fh = open(job_submission_script,'w')
        fh.write('#!/bin/bash\n')
        fh.write("#PBS -N %s.%s\n"%(prefix,run_name))
        fh.write("#PBS -l nodes=1:ppn=%i\n"%processors_per_node)
        fh.write("#PBS -l vmem=%imb\n"%vmem)
        fh.write("#PBS -l walltime=80:00:00\n")
        if base_dir:
            fh.write("cd %s\n"%base_dir)
        else:
            fh.write("cd $PBS_O_WORKDIR\n")
        cmd = 'tophat -p %i -I %i -i %i'%(processors_per_node,
                                          max_intron_size,
                                          min_intron_size)
        if quals:
            cmd = cmd + ' --%s' % quals
            # use --solexa1.3-quals for older Illumina files
            # SRA converts their data to phred33 scaling
        if segment_length:
            cmd = cmd + ' --segment-length %i'% segment_length
        if paired_end and read_distance:
            cmd = cmd + ' -r %i'%read_distance
        fh.write(cmd)
        fh.write(" -o %s"%run_name+'/'+prefix)
        fh.write(" %s %s\n"%(index,f))
        fh.close()
        os.chmod(job_submission_script,0744)
        qsub_fh.write('qsub -o %s.out -e %s.err  %s\n'%(prefix,prefix,job_submission_script))
    qsub_fh.close()
    os.chmod(qsub_script,0744)
    

def main(options,args):
    base_dir = options.base_dir # might be none
    fastq_dir = options.fastq_dir
    run_name = options.run_name
    max_intron_size = options.max_intron_size
    min_intron_size=options.min_intron_size
    index=options.index
    processors_per_node=options.processors_per_node
    vmem=options.vmem
    paired_end=options.paired_end
    read_distance=options.read_distance # might be None
    segment_length=options.segment_length # might be none
    quals = options.quals # might be none
    files = getFastqNames(dir=fastq_dir)
    if len(files)==0:
        raise Error("Found no .fastq files in %s"%fastq_dir)
    writeSubmissionScripts(files=files,
                           run_name=run_name,
                           base_dir=base_dir,
                           quals=quals,
                           processors_per_node=processors_per_node,
                           vmem=vmem,
                           paired_end=paired_end,
                           min_intron_size=min_intron_size,
                           max_intron_size=max_intron_size,
                           segment_length=segment_length,
                           read_distance=read_distance,
                           index=index)
                           
if __name__ == '__main__':
    usage = "%prog [options]\n\nex)\n\n  %prog\n\t--dir_base=/storage/SRP0001/TEST\n\t--fastq_dir=/storage/SRP0001/fastq\n\t--run_name=TEST\n\t--bowtie_index=/storage/bowtieindexes/A_thaliana_Jun_2009\n\nNote that bowtie index needs to give the root name of the indexes.\n\n"
    usage = usage + "For example, assume the bowtie index files for A_thaliana_Jun_2009.fa\nreside in /storage/bowtieindex.\n\nThen --bowtie_index parameter should be /storage/bowtieindex/A_thaliana_Jun_2009."
    parser = optparse.OptionParser(usage)
    parser.add_option("-d","--dir_base",
                      dest="base_dir",
                      help="Full path to directory where run_name output directory will reside [optional, defaults to $PBS_O_WORKDIR])",
                      default=None)
    parser.add_option("-i","--intron_min",
                      dest="min_intron_size",
                      help="Minimum intron size [default is 50]",
                      type="int",
                      default=50)
    parser.add_option("-I","--intron_max",
                      dest="max_intron_size",
                      help="Maximum intron size [default is 2000]",
                      type="int",
                      default=2000)
    parser.add_option("-f","--fastq_dir",
                      dest="fastq_dir",type="string",
                      help="Directory containing fastq files to align [required]",
                      default=None)
    parser.add_option("-r","--run_name",
                      dest="run_name",type="string",
                      help="Name of the tophat run. Also, the name of the directory that will be used for tophat output directories [required]",
                      default=None)
    parser.add_option("-b","--bowtie_index",
                      dest="index",type="string",
                      help="Path to and name of the bowtie index to use for this run [required]",
                      default=None)
    parser.add_option("-p","--processors_per_node",dest="processors_per_node",
                      help="Processors per node [default is 8]",type="int",default=8)
    parser.add_option("-m","--vmem",dest="vmem",
                      help="Memory per node in megabytes [default is 96000]",
                      type="int",default=96000)
    parser.add_option("-s","--segment_length",dest="segment_length",
                      help="Segment length; should be about half read length. Default is fine if reads >= 50 bases.",
                      type="int",default=None)
    parser.add_option("-q","--quals",dest="quals",
                      help="Quality flag to pass to TopHat, e.g., solexa-quals for pre-1.3 Cassava pipeline",
                      default=None,type="string")
    parser.add_option("-P","--pe",dest="paired_end",
                      help="Whether reads are paired end or not [default is false]",
                      default=False,action="store_true")
    parser.add_option("-R","--read_distance",dest="read_distance",
                      help="Expected distance separating reads on fragments. PE only.",
                      default=None,type="int")
    (options,args)=parser.parse_args()
    if not options.index:
        parser.error("Please specify bowtie index.")
    if not options.fastq_dir:
        parser.error("Please specify directory with fastq files.")
    if not options.run_name:
        parser.error("Please specify tophat run name, no whitespace please.")
    main(options,args)
    
                     
