#!/usr/bin/env python

import sys,os,re,optparse

"""Contains functions useful for filtering output of blat and dividing output files into single and multi-mapping queries"""

# pattern for matching PSL lines generated from aligning
# EST or mRNAs onto genomic
gb_pat = re.compile('^(.*)\tgi.*?\|gb\|(.+\.\d+)\|.*?\t(.*)$')

# constants
DEFAULT_COVERAGE=0.9
DEFAULT_IDENTITY=0.95
DEFAULT_FIX_GB_IDS=True
DEFAULT_SPLIT_MAPPERS=True

def calcFracQuality(matches=None,
                    match_repeats=None,
                    mismatches=None,
                    N_bases=None,
                    query_size=None):
    """
    Function: Calculate fraction of query involved in alignment (coverage)
              and fraction of perfect matches in the alignment (identity)
    Args    : matches - int, number of perfect matches
              match_repeats - int, number of query bases that match repetitive
                sequence in the target
              N_bases - int, number of query bases that aligned to N characters
                in the target
              query_size - int, the size of the query sequence

    Returns: two-item tuple of floats (coverage,identity)
    """
    # total number of bases in the alignment, not include gaps
    total_elts = matches+match_repeats+mismatches+N_bases
    coverage = total_elts/(float(query_size))
    identity = (matches+match_repeats+N_bases) / (float(total_elts))
    return (identity,coverage)

def filterByQuality(infn=None,
                    passed_fn=None,
                    rejected_fn=None,
                    coverage=DEFAULT_COVERAGE,
                    identity=DEFAULT_IDENTITY):
    """
    Function: 
    Args    : infn - string, name of file with blat results
              passed - file name prefix to use for alignments that passed the
                       filter
              rejected - file name prefix to use for alignments that did not
                       pass the filter
              coverage - float, minimum coverage (0 <= f <= 1)
              identity - float, minimum identity (0 <= f <= 1)
    Returns: dictionary containing the the number of alignments
             per query that made it through the quality filter
    """
    infh = open(infn,'r')
    passed_fh = open(passed_fn,'w')
    rejected_fh = open(rejected_fn,'w')
    d = {}
    while 1:
        line = infh.readline()
        if not line:
            infh.close()
            break
        toks = line.rstrip().split('\t')
        matches = int(toks[0])
        mismatches = int(toks[1])
        match_repeats = int(toks[2])
        N_bases = int(toks[3])
        query_size = int(toks[10])
        qname = toks[9]
        (i,c) = calcFracQuality(matches=matches,
                                match_repeats=match_repeats,
                                mismatches=mismatches,
                                N_bases=N_bases,
                                query_size=query_size)
        if i >= identity and c >= coverage:
            passed_fh.write(line)
            if not d.has_key(qname):
                d[qname]=1
            else:
                d[qname]=d[qname]+1
        else:
            rejected_fh.write(line)
    passed_fh.close()
    rejected_fh.close()
    return d

def doit(infn=None,
         coverage=DEFAULT_COVERAGE,
         identity=DEFAULT_IDENTITY,
         fix_gb_ids=DEFAULT_FIX_GB_IDS):
    """
    Function: Filter blat results and write out results
    Args    : infn - string, name of file with blat results
              coverage - float, minimum coverage (0 <= f <= 1)
              identity - float, minimum identity (0 <= f <= 1)
              fix_gb_ids - whether or not the query definition line
                requires shortening to ACESSSION.VERSION, e.g.,
                EX654484.1
              split_mappers - boolean, if yes, make two files for the filtered
                  alignments output, one each for multi- and single-mapping
                  alignments
    Returns: dictionary containing the the number of alignments
             per query that made it through the quality filter
    """
    toks = infn.split('.')
    extension = toks[-1]
    passed_fn = ('.').join(toks[:-1])+'.passed.'+extension
    rejected_fn = ('.').join(toks[:-1])+'.rejected'+'.c'+str(coverage*100) + 'i'+str(identity*100)+\
                      '.'+extension
    d = filterByQuality(infn=infn,
                        passed_fn=passed_fn,
                        rejected_fn=rejected_fn,
                        coverage=coverage,
                        identity=identity)
    mm = ('.').join(toks[:-1])+'.mm.'+extension
    sm = ('.').join(toks[:-1])+'.sm.'+extension
    fmm = open(mm,'w')
    fsm = open(sm,'w')
    fh = open(passed_fn,'r')
    while 1:
        line = fh.readline()
        if not line:
            break
        toks = line.split('\t')
        int(toks[0])
        qname = toks[9]                
        if fix_gb_ids:
            m = gb_pat.search(line)
            if not m:
                raise ValueError("No match for: " + line)
            line = str(m.group(1)) + "\t" + str(m.group(2)) + "\t" + str(m.group(3)) + "\n"
        if d[qname]>1:
            fmm.write(line)
        else:
            fsm.write(line)
    return d
   

if __name__ == '__main__':
    usage = "usage: %prog [options] [FILE]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("-c","--coverage",dest="coverage",type="float",action="store",
                      help="minimum acceptable query coverage",default=DEFAULT_COVERAGE)
    parser.add_option("-i","--identity",dest="identity",type="float",action="store",
                      help="minimum acceptable identity",default=DEFAULT_IDENTITY)
    parser.add_option("-g","--fix_gb_ids",action="store_true",dest="fix_gb_ids",
                      default=DEFAULT_FIX_GB_IDS)
    (options,args)=parser.parse_args()
    if len(args)>0:
        infn = args[0]
    else:
        infn=None
    doit(infn=infn,
         coverage=options.coverage,
         identity=options.identity,
         fix_gb_ids=options.fix_gb_ids)
            
    
    
    
