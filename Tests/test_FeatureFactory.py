#!/usr/bin/env python

import unittest
import os,sys

import Mapping.FeatureModel as F
from Mapping.FeatureFactory import makeFeatFromGffFields, cfeat_from_feats, mergeExons

class testmakeFeatFromGffFields(unittest.TestCase):

   # note: it doesn't have the header 
   test_file = 'data/PhytozomeGFF/AT1G07350.2_gene_exons.gff3'

   def setUp(self):
      fh = open(self.test_file)
      self.lines = fh.readlines()
      fh.close()
      self.tokss = []
      for line in self.lines:
      	#self.tokss is a list of each line in the file delimited by tabs. 
         self.tokss.append(line.rstrip().split('\t'))

   def tearDown(self):
      del(self.lines)
      del(self.tokss)
      
   def testMakeFeatFromGffFields(self):
      "makeFeatFromGffFields should make something for every line in the file."
      #feats = (x)result of makeFeatFromGffFields condition applied to self.tokss
      feats = map(lambda x:makeFeatFromGffFields(x), self.tokss)

      self.assertEquals(len(feats),len(self.tokss)) #see if the modified self.tokss is the same as the previous


class test_cfeat_from_feats(unittest.TestCase):

   def testCreateCompoundDNASeqFeature(self):
      "Should be able to access SeqFeature getters/setters"
      #CompoundDNASeqFeature inherits from DNASeqFeature that inherits from SeqFeature.

      subfeats = [F.DNASeqFeature(start=0,length=100,strand=1, seqname='X', producer=None, feat_type="exon"),
                  F.DNASeqFeature(start=101,length=50,strand=1, seqname='X', producer=None, feat_type="exon"),
                  F.DNASeqFeature(start=101,length=100,strand=1, seqname='X', producer=None, feat_type="exon"),
                  F.DNASeqFeature(start=200,length=50,strand=1, seqname='X', producer=None, feat_type="exon")]

      #Default args: (subfeats,display_id=None,group_feat_type='transcript', producer=None, boundary_feat='exon')
      cfeat = cfeat_from_feats(subfeats, display_id="TestID")

      #args set inside method: seqname=subfeats[0].seqname(), producer=producer, type=group_feat_type, feats=subfeats, strand=subfeats[0].strand().
      self.assertEquals(cfeat.getDisplayId(), "TestID")
      self.assertEquals(cfeat.getSeqname(), 'X')
      self.assertEquals(cfeat.getProducer(), None)
      self.assertEquals(cfeat.getFeatType(), 'transcript')
      self.assertEquals(cfeat.getFeats(), subfeats) #subfeats are being sorted at some point.
      self.assertEquals(cfeat.getStrand(), 1)
      self.assertEqual(cfeat.getLength(), 250)
      #self.assertEquals(cfeat.getBoundry(), 'exon') #Not set?


   def testvalueError(self):
      "Should raise a warning if subfeat strand, seqname, or producer do not match: "

      subfeats1 = [F.DNASeqFeature(start=0,length=100,strand=1, seqname='X', producer='tester'),
                  F.DNASeqFeature(start=101,length=50,strand=1, seqname='Z', producer='tester')]

      subfeats2 = [F.DNASeqFeature(start=101,length=100,strand=1, seqname='X', producer='tester'),
                  F.DNASeqFeature(start=101,length=50,strand=-1, seqname='X', producer='tester')]

      subfeats3 = [F.DNASeqFeature(start=101,length=100,strand=1, seqname='X', producer='tester'),
                   F.DNASeqFeature(start=101,length=50,strand=1, seqname='X', producer='Tester')]

      self.assertRaises(ValueError, cfeat_from_feats, subfeats1, display_id="TestID")
      self.assertRaises(ValueError, cfeat_from_feats, subfeats2, display_id="TestID")
      self.assertRaises(ValueError, cfeat_from_feats, subfeats3, display_id="TestID")

class testMergeExons(unittest.TestCase):

   def test_overlapping(self):
      "Output cfeat should be different than original"

      # mergeExons uses removeFeat which edits the actual object, all references to that object also change.
      # for testing two identical objects must be created.
      before_cfeat = F.CompoundDNASeqFeature()
      after_cfeat = F.CompoundDNASeqFeature()
      subfeat0 = F.DNASeqFeature(start=0,length=200,strand=1, feat_type="exon")
      subfeat1 = F.DNASeqFeature(start=0,length=100,strand=1, feat_type="exon")
      before_cfeat.addFeat(subfeat0)
      before_cfeat.addFeat(subfeat1)
      after_cfeat.addFeat(subfeat0)
      after_cfeat.addFeat(subfeat1)

      self.assertNotEqual(mergeExons(after_cfeat), before_cfeat)

   def test_ValueError(self):
      "Should raise a value error for a list that contains no exons"
      cfeat = F.CompoundDNASeqFeature(display_id="Test_cfeat")
      subfeat0 = F.DNASeqFeature(start=50,length=200,strand=1, feat_type="CDs")
      subfeat1 = F.DNASeqFeature(start=0,length=100,strand=1, feat_type="CDs")
      cfeat.addFeat(subfeat0)
      cfeat.addFeat(subfeat1)

      self.assertRaises(ValueError, mergeExons, cfeat)


if __name__ == "__main__":
    unittest.main()
    
