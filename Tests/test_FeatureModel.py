#!/usr/bin/env python

import unittest
import os,sys

import Mapping.FeatureModel as M

class KeyValTest(unittest.TestCase):

   def testEmptyKeyVals(self):
      "If we make a new KeyVals but don't give it any data, getKeyVals should return None"
      kv = M.KeyVal()
      self.assertEquals(kv.getKeyVals(),None)

   def testSetKeyVals(self):
      "If we make a new KeyVals and give a dictionary, getKeyVals should return the same dictionary."
      knownDict = {"First":1}
      kv = M.KeyVal()
      kv.setKeyVals(knownDict)
      self.assertEquals(kv.getKeyVals(),knownDict)

   def testSetKeyVal(self):
      "If we set a value using a key, we should get back that same value using the same key."
      kv = M.KeyVal()
      kv.setKeyVal("Freedom","Fries")
      self.assertEquals(kv.getVal("Freedom"),"Fries")

   def testUnhashableKey(self):
      "If we try to set a value with an unhashable key, a KeyVal should raise a TypeError"
      kv = M.KeyVal()
      self.assertRaises(TypeError,kv.setKeyVal,{},'foo')

   def testNonExistantKey(self):
      "getVal should return None if we ask for a key that isn't there."
      kv = M.KeyVal()
      self.assertEquals(kv.getVal('foo'),None)
      
   def testGetVal(self):
      "getVal should return the same value for a previously set key"
      kv = M.KeyVal()
      val = {}
      kv.setKeyVal('foo',val)
      self.assertEquals(kv.getVal('foo'),val)

class RangeTest(unittest.TestCase):
   
   """Test Data
  s1   s1+len1 s2  s2+len2
   |     |     |     |
    nnnnn nnnnn nnnnn nnnnn nnnnn
       |                   |
      s3                 s3+len3 |
                          s4    s4+len4
   """
   
   """_seqs[0] = (start,length)"""
   _seqs = ((0,5),
            (10,5),
            (3,17),
            (20,5))

   def setUp(self):
      self.object = M.Range(self._seqs[0][0],self._seqs[0][1])
      self.s1 = M.Range(self._seqs[0][0],self._seqs[0][1])
      self.s2 = M.Range(self._seqs[1][0],self._seqs[1][1])         
      self.s3 = M.Range(self._seqs[2][0],self._seqs[2][1])
      self.s4 = M.Range(self._seqs[3][0],self._seqs[3][1])
      var = "Hello World"
      
   def tearDown(self):
      self.object = None
      self.s1 = None
      self.s2 = None
      self.s3 = None
      self.s4 = None

   def testGetStart(self):
      "If we set start in the constructor, getStart should get that same value."
      self.assertEquals(self.object.getStart(),self._seqs[0][0])

   def testDefaultStartIsNone(self):
      "If we create a new Range without giving it any arguments, start should be None"
      r = M.Range()
      self.assertEquals(r.getStart(),None)

   def testDefaultStartIsNone(self):
      "If we create a new Range without giving it any arguments, length should be None"
      r = M.Range()
      self.assertEquals(r.getLength(),None)

   def testSetStart(self):
      "If we make a new Range and then set the start, then getStart should return that same value."
      r = M.Range(start=1,length=10)
      self.object.setStart(3)
      self.assertEquals(self.object.getStart(),3)
   
   def testNegIntBadInputSetStart(self):
      "setStart should raise ValueError when given negative coordinates."
      r = M.Range()
      self.assertRaises(ValueError,r.setStart,-1)

   def testWrongTypeInputSetStart(self):
      "setStart should object when given non-int input."
      r = M.Range()
      self.assertRaises(TypeError,r.setStart,'-1')
   
   def testGetLength(self):
      """Tests if get length returns correct value"""
      self.assertEquals(self.object.getLength(),self._seqs[0][1])

   def testSetLength(self):
      """Tests if set length works"""
      self.object.setLength(3)
      self.assertEquals(self.object.getLength(),3)
   
   def testBadInputSetLength(self):
      """Tests if set start throws exception with bad input"""
      self.assertRaises(ValueError, self.object.setLength,-1)
      
   def testOverlaps(self):
      """Tests if overlaps correctly calculates overlaps"""
      self.assertEquals(self.s1.overlaps(self.s2),False)
      self.assertEquals(self.s1.overlaps(self.s3),True)
      self.assertEquals(self.s2.overlaps(self.s3),True)
      self.assertEquals(self.s2.overlaps(self.s1),False)
      self.assertEquals(self.s3.overlaps(self.s1),True)
      self.assertEquals(self.s3.overlaps(self.s2),True)
      self.assertEquals(self.s3.overlaps(self.s4),False)
      self.assertEquals(self.s4.overlaps(self.s3),False)
            
   def testContains(self):
      """Tests if contains works"""
      self.assertEquals(self.s1.contains(self.s2),False)
      self.assertEquals(self.s2.contains(self.s1),False)
      self.assertEquals(self.s1.contains(self.s3),False)
      self.assertEquals(self.s3.contains(self.s1),False)
      self.assertEquals(self.s2.contains(self.s3),False)
      self.assertEquals(self.s3.contains(self.s2),True)

   def testEquals(self):
      """Tests if equals works"""
      s1 = M.Range(self._seqs[0][0],self._seqs[0][1])
      s2 = M.Range(self._seqs[1][0],self._seqs[1][1])

      self.assertEquals(self.s1.equals(self.s2),False)
      self.assertEquals(self.s1.equals(self.object),True)
      
   def testIsLessThan(self):
		"Tests if isLessThan correctly compares the location of self to r"
		self.assertEquals(self.s1.isLessThan(self.s2),True)
		self.assertEquals(self.s1.isLessThan(self.s3),False)
		self.assertEquals(self.s1.isLessThan(self.s4),True)
		self.assertEquals(self.s2.isLessThan(self.s3),False)
		self.assertEquals(self.s2.isLessThan(self.s4),True)
		self.assertEquals(self.s3.isLessThan(self.s4),False)
		self.assertEquals(self.s4.isLessThan(self.s1),False)
		self.assertEquals(self.s4.isLessThan(self.s2),False)
	
   def testCheckType(self):
      "Raise a new TypeError with the given message if two values are not the same type"
      #If you want it to throw an error to see the given error message, uncomment below
      #self.assertRaises(TypeError, self.object.checkType("string", int(1) , "I am an error message")  )
      self.assertRaises(TypeError, self.object.checkType, "string", int(1) , "I am an error message"  )
	
   def testCheckCoord(self):
      "Check val is non-negative. If not, raise a ValueError using the given message."
      self.assertRaises(ValueError, self.object.checkCoord, -1, "Value is less than 0" )
	

class SeqFeatureTest(unittest.TestCase):

   def testDefaultsAreNone(self):
      "If we make a new SeqFeature with no arguments, initial values should be None"
      s = M.SeqFeature()
      self.assertEquals(s.getDisplayId(),None)
      self.assertEquals(s.getSeqname(),None)
      self.assertEquals(s.getFeatType(),None)
      self.assertEquals(s.getDisplayId(),None)
      self.assertEquals(s.getStart(),None)

   def testDisplayId(self):
      "Test getter/setter for DisplayId"
      "If we create a SeqFeature, set its display id, and then reset to None, the new value should be None"
      s = M.SeqFeature()
      s.setDisplayId('foo')
      self.assertEquals(s.getDisplayId(),'foo')
      s.setDisplayId(None)
      self.assertEquals(s.getDisplayId(),None)
      
   def testGetSeqname(self):
      "Test getter/setter for Seqname"
      s = M.SeqFeature()
      s.setSeqname("name")
      self.assertEquals(s.getSeqname(), "name")
      s.setSeqname(None)
      self.assertEquals(s.getSeqname(), None)
      
   def testProducer(self):
      "Test getter/setter for Producer"
      s = M.SeqFeature()
      s.setProducer("name")
      self.assertEquals(s.getProducer(), "name")
      s.setProducer(None)
      self.assertEquals(s.getProducer(), None) 
      
   def testFeatType(self):
      "Test getter/setter for FeatType"
      s = M.SeqFeature()
      s.setFeatType("gene")
      self.assertEquals(s.getFeatType(), "gene")
      s.setFeatType(None)
      self.assertEquals(s.getFeatType(), None) 


class DNASeqFeatureTest(unittest.TestCase):

   def testWrongStrandValue(self):
      "setStrand should object if given a value not in [1,-1,None]"
      s = M.DNASeqFeature()
      self.assertRaises(ValueError,s.setStrand,'foo')

   def testContainsNoStrand(self):
      "If DNASeqFeature contains another and neither has a strand, contains should return True"
      outer = M.DNASeqFeature(start=0,length=100)
      inner = M.DNASeqFeature(start=0,length=50)
      self.assertEquals(outer.contains(inner),True)

   def testContainsDiffStrand(self):
      "If DNASeqFeature contains another but they have different strands, contains should return False"
      outer = M.DNASeqFeature(start=0,length=100,strand=1)
      inner = M.DNASeqFeature(start=0,length=50,strand=-1)
      self.assertEquals(outer.contains(inner),False)

   def testEqualsSameStartLengthStrand(self):
      "If DNASeqFeature has same coordinates as another and the same strand, equals should return True"
      this = M.DNASeqFeature(start=0,length=100,strand=1)
      that = M.DNASeqFeature(start=0,length=100,strand=1)
      self.assertEquals(this.equals(that),True)

   def testEqualsDiffStrand(self):
      "If DNASeqFeature has same coordinates as another and different strand, equals should return False"
      this = M.DNASeqFeature(start=0,length=100,strand=-1)
      that = M.DNASeqFeature(start=0,length=100,strand=1)
      self.assertEquals(this.equals(that),False)
      
   def testScore(self):
      "Test getter/setter for Score"
      s = M.DNASeqFeature()
      s.setScore("foo")
      self.assertEquals(s.getScore(), "foo")
      s.setScore(None)
      self.assertEquals(s.getScore(), None) 
      
   def testOverlaps(self):
      "Test if overlaps correctly identifies overlapping sequences"
      s1 = M.DNASeqFeature(start=0,length=100,strand=1)
      s2 = M.DNASeqFeature(start=0,length=100,strand=1)  
      s3 = M.DNASeqFeature(start=101,length=50,strand=1)
      s4 = M.DNASeqFeature(start=101,length=50,strand=-1) 
      self.assertEquals(s1.overlaps(s2), True) 
      self.assertEquals(s1.overlaps(s3), False) 
      self.assertEquals(s3.overlaps(s4), False) 
       
   def testIsFivePrimeOf(self):
      "Test isFivePrimeOf"
      s1 = M.DNASeqFeature(start=0,length=100,strand=1)
      s2 = M.DNASeqFeature(start=101,length=50,strand=1)  
      s3 = M.DNASeqFeature(start=101,length=100,strand=None)
      s4 = M.DNASeqFeature(start=101,length=50,strand=-1) 
      self.assertEquals(s1.isFivePrimeOf(s2), True)
      self.assertEquals(s1.isFivePrimeOf(s3), True)
      self.assertEquals(s2.isFivePrimeOf(s1), False)
      self.assertEquals(s1.isFivePrimeOf(s4), False)
   
   
   def testIsThreePrimeOf(self):
      "Test isThreePrimeOf"
      s1 = M.DNASeqFeature(start=0,length=100,strand=1)
      s2 = M.DNASeqFeature(start=101,length=50,strand=1)  
      s3 = M.DNASeqFeature(start=101,length=100,strand=None)
      s4 = M.DNASeqFeature(start=101,length=50,strand=-1) 
      self.assertEquals(s1.isThreePrimeOf(s2), False)
      self.assertEquals(s3.isThreePrimeOf(s1), True)
      self.assertEquals(s2.isThreePrimeOf(s1), True)
      self.assertEquals(s1.isThreePrimeOf(s4), False)      
      
   

class CompoundDNASeqFeatureTest(unittest.TestCase):

   feat_type = 'transcript'
   seqname='X'
   start = 0
   length = 9000
   display_id = 'A'
   strand = 1
   producer='tester'
   strand = 1

   def setUp(self):
      a = M.CompoundDNASeqFeature(start=self.start,
                                  length=self.length,
                                  feat_type=self.feat_type,
                                  seqname=self.seqname,
                                  producer=self.producer,
                                  strand=self.strand,
                                  display_id=self.display_id)
      self.feat = a

   def testCreateCompoundDNASeqFeature(self):
      "Test constructor and getters"
      a = self.feat
      self.assertEquals(a.getLength(),self.length)
      self.assertEquals(a.getStart(),self.start)
      self.assertEquals(a.getFeatType(),self.feat_type)
      self.assertEquals(a.getStrand(),self.strand)
      self.assertEquals(a.getProducer(),self.producer)
      self.assertEquals(a.getDisplayId(),self.display_id)

   def testDefaultsAreNone(self):
      "If we make a new CompoundDNASeqFeature with no arguments, initial values should be None"
      s = M.CompoundDNASeqFeature()
      self.assertEquals(s.getDisplayId(),None)
      self.assertEquals(s.getSeqname(),None)
      self.assertEquals(s.getFeatType(),None)
      self.assertEquals(s.getDisplayId(),None)
      self.assertEquals(s.getStart(),None)

   def testSortedVariableChanges(self):
      "When we call getSortedFeats for the first time, _sorted variable switches from False to True."
      s = M.CompoundDNASeqFeature()
      subfeat0 = M.DNASeqFeature(start=500,length=200,strand=1) # third
      subfeat1 = M.DNASeqFeature(start=0,length=100,strand=1) # first
      subfeat2 = M.DNASeqFeature(start=200,length=100,strand=1) # second
      s.addFeat(subfeat0)
      s.addFeat(subfeat1)
      s.addFeat(subfeat2)
      self.assertEquals(s._sorted,False)
      feats=s.getSortedFeats()
      self.assertEquals(s._sorted,True)

   def testGetFeats(self):
      "Test safety flag in addFeats and feat_type flag in getFeats"
      s = M.CompoundDNASeqFeature()
      subfeat0 = M.DNASeqFeature(start=500,length=200,feat_type="CDS", strand=1, safe=1)
      subfeat1 = M.DNASeqFeature(start=0,length=100,feat_type="mRNA", strand=1, safe=1)
      subfeat2 = M.DNASeqFeature(start=200,length=100,feat_type="CDS", strand=1, safe=1)
      s.addFeat(subfeat0)
      s.addFeat(subfeat1)
      s.addFeat(subfeat2)
      feats = [subfeat0, subfeat2]
      gotfeats= s.getFeats(feat_type="CDS")
      self.assertEquals(gotfeats,feats)

   def testRemoveFeats(self):
      "Test removeFeats"
      s = M.CompoundDNASeqFeature()
      subfeat0 = M.DNASeqFeature(start=500,length=200, strand = 1)
      subfeat1 = M.DNASeqFeature(start=0,length=100, strand = 1)
      subfeat2 = M.DNASeqFeature(start=200,length=100, strand = 1)
      s.addFeat(subfeat0)
      s.addFeat(subfeat1)
      s.addFeat(subfeat2)
      s.removeFeat(subfeat1)
      feats = [subfeat0, subfeat2]
      gotfeats= s.getFeats()
      self.assertEquals(gotfeats,feats)

   def testGetSortedFeats(self):
      "getSortedFeats should return subfeatures sorted by start."
      s = M.CompoundDNASeqFeature()
      subfeat0 = M.DNASeqFeature(start=500,length=200,strand=1) # third
      subfeat1 = M.DNASeqFeature(start=0,length=100,strand=1) # first
      subfeat2 = M.DNASeqFeature(start=200,length=100,strand=1) # second
      s.addFeat(subfeat0)
      s.addFeat(subfeat1)
      s.addFeat(subfeat2)
      feats=s.getSortedFeats()
      self.assertEquals(feats[0],subfeat1)
      self.assertEquals(feats[1],subfeat2)
      self.assertEquals(feats[2],subfeat0)

   def testUseSubFeatCoords(self):
      "If we make a new CompoundDNASeqFeature, add a subfeature to it, and call useSubFeatCoords, the CompoundDNASeqFeature should acquire the coordinates and strand of the subfeature."
      s = M.CompoundDNASeqFeature()
      subfeat0 = M.DNASeqFeature(start=500,length=200,strand=1)
      subfeat1 = M.DNASeqFeature(start=0,length=100,strand=1)
      subfeat2 = M.DNASeqFeature(start=200,length=100,strand=1)
      s.addFeat(subfeat0)
      s.addFeat(subfeat1)
      s.addFeat(subfeat2)
      s.useSubFeatCoords()
      self.assertEquals(s.getStart(),subfeat1.getStart())
      self.assertEquals(s.getStrand(),subfeat1.getStrand())
      self.assertEquals(s.getEnd(),subfeat0.getEnd())

   def testGetTransStartandEnd(self):
      #Tests if getTranslStartAndEnd returns a tuple of the first start and the last end in this compoundDNASeqFeature
      s = M.CompoundDNASeqFeature()
      subfeat0 = M.DNASeqFeature(start=500,length=200,feat_type="CDS")
      subfeat1 = M.DNASeqFeature(start=0,length=100,feat_type="CDS")
      subfeat2 = M.DNASeqFeature(start=200,length=100,feat_type="CDS")
      s.addFeat(subfeat0)
      s.addFeat(subfeat1)
      s.addFeat(subfeat2)
      self.assertEquals(s.getTranslStartAndEnd(), (subfeat1.getStart(), (subfeat0.getStart() + subfeat0.getLength() ) ) )


    
if __name__ == "__main__":
    unittest.main()
