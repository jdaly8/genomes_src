#!/usr/bin/env python
# This code is part of the Biopython distribution and governed by its
# license. Please see the LICENSE file that should have been included
# as part of this package.
"""Run a set of PyUnit-based regression tests.
 
This will find all modules whose name is "test_*.py" in the test
directory, and run them. Various command line options provide
additional facilities.
 
Command line options:
 
--help -- show usage info
-g;--generate -- write the output file for a test instead of comparing it.
The name of the test to write the output for must be
specified.
-v;--verbose -- run tests with higher verbosity (does not affect our
print-and-compare style unit tests).
<test_name> -- supply the name of one (or more) tests to be run.
The .py file extension is optional.
doctest -- run the docstring tests.
By default, all tests are run.
"""

# The default verbosity (not verbose)
VERBOSITY = 0
 
# standard modules
import sys
import cStringIO
import os
import re
import getopt
import time
import traceback
import unittest
import doctest
import distutils.util
 
 
def main(argv):
    # insert our paths in sys.path
    test_path = sys.path[0] or "."
    source_path = os.path.abspath("%s/.." % test_path)
    sys.path.insert(1, source_path)
    # get the command line options
    try:
        opts, args = getopt.getopt(argv, 'gv', ["generate", "verbose",
            "doctest", "help"])
    except getopt.error, msg:
        print msg
        print __doc__
        return 2
 
    verbosity = VERBOSITY
 
    # deal with the options
    for o, a in opts:
        if o == "--help":
            print __doc__
            return 0
        if o == "-g" or o == "--generate":
            if len(args) > 1:
                print "Only one argument (the test name) needed for generate"
                print __doc__
                return 2


            elif len(args) == 0:
                print "No test name specified to generate output for."
                print __doc__
                return 2
            # strip off .py if it was included
            if args[0][-3:] == ".py":
                args[0] = args[0][:-3]
 
            test = ComparisonTestCase(args[0])
            test.generate_output()
            return 0
 
        if o == "-v" or o == "--verbose":
            verbosity = 2
 
    # deal with the arguments, which should be names of tests to run
    for arg_num in range(len(args)):
        # strip off the .py if it was included
        if args[arg_num][-3:] == ".py":
            args[arg_num] = args[arg_num][:-3]
 
    # run the tests
    runner = TestRunner(args, verbosity)
    runner.run()
 
 
class TestRunner(unittest.TextTestRunner):
 
    if __name__ == '__main__':
        file = sys.argv[0]
    else:
        file = __file__
    testdir = os.path.dirname(file) or os.curdir
 
    def __init__(self, tests=[], verbosity=0):
        # if no tests were specified to run, we run them all
        self.tests = tests
        if not self.tests:
            # Make a list of all applicable test modules.
            names = os.listdir(TestRunner.testdir)
            for name in names:
                if name[:5] == "test_" and name[-3:] == ".py":
                    self.tests.append(name[:-3])
            self.tests.sort()
        stream = cStringIO.StringIO()
        unittest.TextTestRunner.__init__(self, stream,
                verbosity=verbosity)
 
    def runTest(self, name):
        result = self._makeResult()
        output = cStringIO.StringIO()
        # Run the actual test inside a try/except to catch import errors.
        # Have to do a nested try because try/except/except/finally requires
        # python 2.5+
        try:
            try:
                stdout = sys.stdout
                sys.stdout = output
                if name.startswith("test_"):
                    sys.stderr.write("%s ... " % name)
                    #It's either a unittest or a print-and-compare test
                    suite = unittest.TestLoader().loadTestsFromName(name)
                    if suite.countTestCases()==0:
                        # This is a print-and-compare test instead of a
                        # unittest-type test.
                        test = ComparisonTestCase(name, output)
                        suite = unittest.TestSuite([test])
                else:
                    #It's a doc test
                    sys.stderr.write("%s docstring test ... " % name)
                    #Can't use fromlist=name.split(".") until python 2.5+
                    module = __import__(name, None, None, name.split("."))
                    suite = doctest.DocTestSuite(module)
                    del module
                suite.run(result)
                if result.wasSuccessful():
                    sys.stderr.write("ok\n")
                    return True
                else:
                    sys.stderr.write("FAIL\n")
                    result.printErrors()
                return False
            except Exception, msg:
                # This happened during the import
                sys.stderr.write("ERROR\n")
                result.stream.write(result.separator1+"\n")
                result.stream.write("ERROR: %s\n" % name)
                result.stream.write(result.separator2+"\n")
                result.stream.write(traceback.format_exc())
                return False
            except KeyboardInterrupt, err:
                # Want to allow this, and abort the test
                # (see below for special case)
                raise err
            except:
                # This happens in Jython with java.lang.ClassFormatError:
                # Invalid method Code length ...
                sys.stderr.write("ERROR\n")
                result.stream.write(result.separator1+"\n")
                result.stream.write("ERROR: %s\n" % name)
                result.stream.write(result.separator2+"\n")
                result.stream.write(traceback.format_exc())
                return False
        finally:
            sys.stdout = stdout
 
    def run(self):
        failures = 0
        startTime = time.time()
        for test in self.tests:
            ok = self.runTest(test)
            if not ok:
                failures += 1
        total = len(self.tests)
        stopTime = time.time()
        timeTaken = stopTime - startTime
        sys.stderr.write(self.stream.getvalue())
        sys.stderr.write('-' * 70 + "\n")
        sys.stderr.write("Ran %d test%s in %.3f seconds\n" %
                            (total, total != 1 and "s" or "", timeTaken))
        sys.stderr.write("\n")
        if failures:
            sys.stderr.write("FAILED (failures = %d)\n" % failures)
 
 
if __name__ == "__main__":
    #Don't do a sys.exit(...) as it isn't nice if run from IDLE.
    main(sys.argv[1:])
 
