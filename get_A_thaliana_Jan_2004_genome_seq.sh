#!/bin/bash

files="ATH1_chr1.1con.01222004
ATH1_chr2.1con.01222004
ATH1_chr3.1con.01222004
ATH1_chr4.1con.01222004
ATH1_chr5.1con.01222004
ATH1_chloroplast.1con.01072002
mitochondrial_genomic_sequence"

g=A_thaliana_Jan_2004
u=ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/
for f in $files
do
    if [ ! -s $f ]; then
        wget $u/$f
    fi
done
for f in $files
do
    cat $f >> tmp.fa
done

cat tmp.fa | sed '/^>CHR\(.\)v01212004/ s//>chr\1/' | sed '/^>chloroplast/ s//>chrC/' | sed '/^>m.*$/ s//>chrM/' > $g.fa
faToTwoBit $g.fa A_thaliana_Jan_2004.2bit
twoBitInfo $g.2bit genome.txt
rm $g.fa tmp.fa
