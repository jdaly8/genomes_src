"""
Functions for dealing with Distributed Annotation Server at UCSC

To see how it works, fire up the interpreter and type:


>>> import das_utils as du
>>> du.test()
plus strand:
tttggctcacctatcctgtgatgattttccttgctcaggaaataggatgg
minus strand:
ccatcctatttcctgagcaaggaaaatcatcacaggataggtgagccaaa

"""

# example URL to retrieve some sequence bases data
# http://genome.cse.ucsc.edu/cgi-bin/das/hg17/dna?segment=1:158363860,158380368


# first, import useful modules (like libraries) and methods 
import sys,os,re,string,urllib

# third-party XML tools
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


# the base URL for UCSC DAS
# to see all the goodies it can deliver, access
# http://genome.cse.ucsc.edu/cgi-bin/das/dsn
das_url = 'http://genome.cse.ucsc.edu/cgi-bin/das'

# because DAS delivers data in XML format, we need to
# parse XML
# Fortunately, this is easy to do -- we just make use of code
# that others have written and adapt it to our own uses.
class SequenceHandler(ContentHandler):
    # we create a subclass of ContentHandler, over-writing
    # methods that are invoked whenever we encounter
    # "events" - such as the start of an element

    _seq = ""
    _len = None
    _on_seq = False
    
    def startElement(self,name,attrs):
        # whenever we encounter an element called "DNA",
        # we take a look at its attributes
        # if the "feature" has an "length" attribute, we assume it
        # signals that some sequence bases are on the way
        if name == 'DNA':
            if attrs.has_key('length'):
                leng = attrs['length']
                self._len = int(leng)
                self._on_seq = True
        # print name

    # if we encounter any character data (could happen anywhere
    # in the parse) check to see if we have already encountered
    # a DNA element (self._on_seq)
    # if yes, then get the character data, removing any whitespace
    # on the end, just in case...
    def characters(self,content):
        if self._on_seq:
            chars = content.rstrip()
            if len(chars) > 0:
                self._seq = self._seq + chars

    # we don't use this yet..but useful just in case
    def get_len(self):
        return self._len

    # return the sequence
    def get_sequence(self):
        return self._seq
    
# let's define a method that builds a URL for
# accessing data from the UCSC Genome Informatics Distributed
# Annotation Server
def make_sequence_url(v='hg17',start=None,end=None,seqid=None):
    """
    Function: Create a URL we need to get a sequence segment
              from the UCSC DAS.
    Returns : a URL
    Args    : v - the genome version we want
              start - the start position (one-based)
              end - the end position (one-based)
              seqid - the name of the chromosome we want
    """
    u = das_url + '/' + v + '/dna?segment='+seqid+':'+start+','+end
    return u

def get_sequence(v='hg17',start=None,end=None,seqid=None,revcomp=0):
    """
    Function: Retrieve a segment of DNA sequence (plus strand)
    Returns : a sequence string
    Args    : v - genome version
              start - start position, one-based, an int
              end - end position, one-based, an int
              seqid - chromosome name, the sequence id, a string
                      e.g., 1,2,3,X etc.
              revcomp - if 1 (or any value that evalutaes to True),
                        return the reverse complement of the requested
                        region, which is equivalent to getting the minus
                        strand sequence.
                        default is 0, i.e., return the plus strand
                        sequence

    Note: to use the revcomp feature, you need to install BioPython!
    If you don't have BioPython, this will still work, just don't
    use the revcomp=1 option.
    """
    import urllib
    u = make_sequence_url(v=v,start=repr(start),
                          end=repr(end),seqid=seqid)
    # print u
    fh = urllib.urlopen(u)
    parser = make_parser()
    handler = SequenceHandler()
    parser.setContentHandler(handler)
    parser.parse(fh)
    seq = handler.get_sequence()
    seq = seq.encode('ascii') # biopython hates unicode
    if revcomp:
        # biopython (www.biopython.org)
        from Bio.Alphabet import IUPAC
        from Bio.Seq import Seq
        bioseq = Seq(seq,IUPAC.unambiguous_dna)
        bioseq = bioseq.reverse_complement()
        seq = bioseq.tostring()
    return seq

def test():
    """
    Function: Just test  
    Returns :
    Args    :
    """
    v = 'hg18'
    start = 158363860
    # note that DAS uses one-based coordinate system
    end = start + 49
    start = repr(start)
    end = repr(end)
    seqid = '1'
    seq = get_sequence(v=v,start=start,end=end,seqid=seqid)
    print "plus strand:"
    print seq
    print "minus strand:"
    try:
        seq = get_sequence(v=v,start=start,end=end,seqid=seqid,revcomp=1)
        print seq
    except Exception:
        print "Sorry, you don't appear to have installed BioPython."
        print "To get the reverse strand sequence, install BioPython and try again."
        

    
    
def test_seq(b):
    """
    Function: Check whether the given sequence string has any
              fishy characters.
    Returns : 
    Args    : a sequence string

    If finds something weird, prints it and returns and True.
    Otherwise, prints nothing and returns None.
    """
    seq = b.tostring().lower()
    got_weird_one = 0
    for char in seq:
        if char not in ['a','t','c','g']:
            print char
            got_weird_one = 1
    if got_weird_one:
        return True
    else:
        return None

# just a stub for copying and pasting doc
# string
def zdummy():
    """
    Function:
    Returns :
    Args    :
    """
    pass
        
